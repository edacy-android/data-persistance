package com.example.session4_datapersistance.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.session4_datapersistance.R;
import com.example.session4_datapersistance.firebase_database.FirebaseDataHelper;
import com.example.session4_datapersistance.room_database.AppDatabase;
import com.example.session4_datapersistance.room_database.Talent;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class InscriptionActivity extends AppCompatActivity {

    private AppDatabase database;
    private FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        final EditText editPrenom = findViewById(R.id.prenom);
        final EditText editNom = findViewById(R.id.nom);
        final EditText editAge = findViewById(R.id.age);
        final EditText editCohorte = findViewById(R.id.cohorte);


        database = AppDatabase.getInstance(this);


        findViewById(R.id.s_inscrire).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String prenom = getEditTextContent(editPrenom);
                String nom = getEditTextContent(editNom);
                String age = getEditTextContent(editAge);
                String cohorte = getEditTextContent(editCohorte);
                //TODO verifier champ par champ pour pouvoir personnaliser les erreurs
                if (!prenom.equals("") && !nom.equals("") && !age.equals("") && !cohorte.equals("")) {
                    Talent talent = new Talent(prenom, nom, Integer.parseInt(age), cohorte);
                    //save data in room database
                    Long result = database.talentDao()
                            .create(talent);
                    //save data in firebase
                    new FirebaseDataHelper().save(talent);
                    if (result != null)
                        showToast("Success");
                    else
                        showToast("Failure");
                }
                else {
                    editAge.setError("");
                    editNom.setError("");
                    editPrenom.setError("");
                    editCohorte.setError("");
                }
            }
        });

        findViewById(R.id.afficher).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InscriptionActivity.this, ListTalentActivity.class);
                startActivity(intent);

            }
        });

    }


    private String getEditTextContent(EditText editText) {
        return editText.getText().toString();
    }

    private void showToast(String message) {
        Toast.makeText(this,
                message, Toast.LENGTH_LONG).show();
    }
}
