package com.example.session4_datapersistance.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.session4_datapersistance.R;
import com.example.session4_datapersistance.firebase_database.FirebaseDataHelper;
import com.example.session4_datapersistance.room_database.AppDatabase;
import com.example.session4_datapersistance.adapter.ListTalentAdapter;
import com.example.session4_datapersistance.room_database.Talent;

import java.util.ArrayList;
import java.util.List;

public class ListTalentActivity extends AppCompatActivity implements ListTalentAdapter.ListItemClickListener {

    private AppDatabase appDatabase;
    private ListTalentAdapter adapter;
    private static final String TAG = "ListTalentActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_talent);

        ListView listView = findViewById(R.id.listView);
        appDatabase = AppDatabase.getInstance(this);
        final List<Talent> talentList = new ArrayList<>(); //appDatabase.talentDao().lister();

        adapter = new ListTalentAdapter(this,
                R.layout.talent_info, talentList, this);
        new FirebaseDataHelper().lister(new FirebaseDataHelper.DataStatus() {
            @Override
            public void dataLoaded(List<Talent> talents) {
                talentList.clear();
                talentList.addAll(talents);
                Log.d(TAG, "onCreate: " + talentList.size());
                adapter.notifyDataSetChanged();
            }
        });


        listView.setAdapter(adapter);


    }


    //TODO refresh listView after deletion
    @Override
    public void onItemLongClicked(Talent talent) {
        appDatabase.talentDao().delete(talent);
        adapter.notifyDataSetChanged();
    }

    //TODO changer cette methode pour fournir une boite de dialogue pour modifier les infos
    @Override
    public void onItemClicked(Talent talent) {
        talent.setNom("Dia");
        talent.setPrenom(talent.getPrenom());
        talent.setAge(34);
        appDatabase.talentDao().update(talent);
        adapter.notifyDataSetChanged();
    }
}
