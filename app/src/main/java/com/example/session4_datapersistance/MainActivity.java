package com.example.session4_datapersistance;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.session4_datapersistance.files.Files;
import com.example.session4_datapersistance.preferences.SharedPrefsHelper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Files files = new Files(this);
//        files.writeInternalStorage("user.txt", "Salut tout le monde");
//        files.readFile("user.txt");
//
//        files.writeExternalStorage("user.txt", "Salut tout le monde");

        final EditText editTextLogin = findViewById(R.id.login);

        final SharedPrefsHelper prefsHelper = new SharedPrefsHelper(this);
        if (prefsHelper.getLogin() != null) {
            editTextLogin.setText(prefsHelper.getLogin());
        }

        findViewById(R.id.se_connecter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prefsHelper.putLogin(editTextLogin.getText().toString());
            }
        });
    }
}
