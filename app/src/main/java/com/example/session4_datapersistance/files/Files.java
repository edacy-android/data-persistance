package com.example.session4_datapersistance.files;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Cette classe peut servir d'utilitaire pour creer/lire des fichiers
 *
 */
public class Files {

    private Context context;
    public Files(Context context) {
        this.context = context;
    }

    /**
     * Ecrire dans la memoire interne
     * @param filename nom du fichier
     * @param contenu du fichier
     * @return  true si l'ecriture reussit false sinon
     */
    public boolean writeInternalStorage(String filename, String contenu) {
        FileOutputStream stream;
        try {
            stream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            stream.write(contenu.getBytes());
            stream.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return true;
        }

    }

    /**
     *
     * @param filename nom du fichier
     * @return le contenu du fichier
     */
    public FileInputStream readFile(String filename) {
        try {
            return context.openFileInput(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Ecrire dans la memoire externe
     * @param filename nom du fichier
     * @param contenu contenu du fichier
     */
    public void writeExternalStorage(String filename, String contenu) {
        File file = new File(Environment.getExternalStorageDirectory(), "mes fichiers");
        File monfichier = new File(Environment.getExternalStorageDirectory() + file.getAbsolutePath(), filename);
        file.mkdir();
        try {
            monfichier.createNewFile();
            FileOutputStream stream = new FileOutputStream(file);
            stream.write(contenu.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
