package com.example.session4_datapersistance.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.session4_datapersistance.MainActivity;

/**
 * Cette classe est utilisee {@link MainActivity}
 * pour la sauvegarde des preferences
 */
public class SharedPrefsHelper {

    private SharedPreferences preferences;

    private final static String PREFS_LOGIN = "LOGIN_PREFS";
    private final static String LOGIN_KEY = "LOGIN_KEY";

    public SharedPrefsHelper (Context context) {
        preferences = context.getSharedPreferences(PREFS_LOGIN, Context.MODE_PRIVATE);
    }

    public void putLogin(String login) {

        preferences.edit().putString(LOGIN_KEY, login).apply();
    }

    public String getLogin() {
        return preferences.getString(LOGIN_KEY, null);
    }
}
