package com.example.session4_datapersistance.room_database;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "talent")
public class Talent {

    /**
     * autoGrenerate equivaut a auto_increment dans les langages relationnels
     */
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String nom;
    private String prenom;
    private int age;
    private String cohorte;

    public Talent() {}

    public Talent(String prenom, String nom, int age, String cohorte) {
        this.prenom = prenom;
        this.nom = nom;
        this.age = age;
        this.cohorte = cohorte;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCohorte() {
        return cohorte;
    }

    public void setCohorte(String cohorte) {
        this.cohorte = cohorte;
    }
}
