package com.example.session4_datapersistance.room_database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface TalentDao {

    /**
     *
     * @param talent
     * @return the number inserted lines
     */
    @Insert
    Long create(Talent talent);

    @Query("SELECT * FROM talent")
    List<Talent> lister();

    @Delete
    void delete(Talent talent);

    @Update
    void update(Talent talent);
    @Query("SELECT * FROM talent GROUP BY cohorte")
    List<Talent> listTalentGroupByCohote();

    @Query("SELECT * FROM talent WHERE cohorte = :cohorte")
    List<Talent> listTalentOfOneCohorte(String cohorte);
}
