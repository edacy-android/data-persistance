package com.example.session4_datapersistance.room_database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = Talent.class, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase appDatabase;

    public abstract TalentDao talentDao();

    public static AppDatabase getInstance(Context context) {

        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(context,
                    AppDatabase.class, "talent_db")
                    .allowMainThreadQueries()
                    .build();
        }
        return appDatabase;
    }
}
