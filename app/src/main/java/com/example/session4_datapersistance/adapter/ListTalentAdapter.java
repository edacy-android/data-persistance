package com.example.session4_datapersistance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.session4_datapersistance.R;
import com.example.session4_datapersistance.room_database.Talent;

import java.util.List;

public class ListTalentAdapter extends ArrayAdapter<Talent> {
    private LayoutInflater mInflater;
    private List<Talent> talents;
    private ListItemClickListener mListener;

    public ListTalentAdapter(Context context, int resource,
                             List<Talent> talentList, ListItemClickListener listener) {
        super(context, resource);
        this.mInflater = LayoutInflater.from(context);
        talents = talentList;
        mListener = listener;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        TalentViewHolder holder;
        if (convertView == null) {
            holder = new TalentViewHolder();
            convertView = mInflater.inflate(R.layout.talent_info, parent, false);
            holder.tvAge = convertView.findViewById(R.id.age);
            holder.tvPrenomEtNom = convertView.findViewById(R.id.prenom);
            holder.tvCohorte = convertView.findViewById(R.id.cohorte);
            convertView.setTag(holder);
        } else {
            holder = (TalentViewHolder) convertView.getTag();
        }
        final Talent talent = talents.get(position);
        String prenomEtNom = talent.getPrenom().concat(" " +talent.getNom());
        int age = talent.getAge();
        String cohorte = talent.getCohorte();
        holder.tvPrenomEtNom.setText(prenomEtNom);
        holder.tvAge.setText(String.valueOf(age).concat(" ans"));
        holder.tvCohorte.setText("Cohorte ".concat(cohorte));
        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.onItemLongClicked(talent);
                return true;
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClicked(talent);
            }
        });
        return convertView;
    }

    @Override
    public int getCount() {
        return talents.size();
    }

    class TalentViewHolder {
        TextView tvPrenomEtNom, tvAge, tvCohorte;

    }


    public interface ListItemClickListener {
        /**
         * Gestion de long click sur un item de la liste
         * Suppression de l'element
         * @param talent
         */
        void onItemLongClicked(Talent talent);

        /**
         * Modification de l'element
         * @param talent
         */
        void onItemClicked(Talent talent);


    }
}
