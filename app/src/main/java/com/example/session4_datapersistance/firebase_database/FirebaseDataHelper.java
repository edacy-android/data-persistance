package com.example.session4_datapersistance.firebase_database;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.session4_datapersistance.room_database.Talent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseDataHelper {

    private static final String TAG = "FirebaseDataHelper";
    private DatabaseReference databaseReference;

    public FirebaseDataHelper() {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference  = firebaseDatabase.getReference("talents");

    }

    public void save(Talent talent) {
        String id = databaseReference.push().getKey();
//        talent.setId(id);
        databaseReference.child(id).setValue(talent);
    }

    //TODO change this to async
    public void lister(final DataStatus dataStatus) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Talent> talentList = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Talent talent = ds.getValue(Talent.class);

                    talentList.add(talent);

                }
                Log.d(TAG, "onDataChange: " + talentList.size());
                dataStatus.dataLoaded(talentList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public interface DataStatus {
        void dataLoaded(List<Talent> talents);
    }
}
